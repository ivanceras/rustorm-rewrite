use dao::TableName;
use column::Column;
use dao::ColumnName;
use types::SqlType;

#[derive(Debug, PartialEq, Clone)]
pub struct Table {
    pub name: TableName,

    /// comment of this table
    pub comment: Option<String>,

    /// columns of this table
    pub columns: Vec<Column>,

    /// views can also be generated
    pub is_view: bool,

    pub table_key: Vec<TableKey>,
}

impl Table {
    pub fn complete_name(&self) -> String {
        self.name.complete_name()
    }

    pub fn get_primary_column_names(&self) -> Vec<&ColumnName> {
        let mut primary: Vec<&ColumnName> = vec![];
        for key in &self.table_key {
            match *key {
                TableKey::PrimaryKey(ref pk) => for col in &pk.columns {
                    primary.push(col)
                },
                _ => (),
            }
        }
        primary.sort_by(|a,b|a.name.cmp(&b.name));
        primary
    }

    fn get_primary_columns(&self) -> Vec<&Column> {
        self.get_primary_column_names()
            .iter()
            .filter_map(|column_name| self.get_column(column_name))
            .collect()
    }

    pub fn get_primary_column_types(&self) -> Vec<&SqlType> {
        self.get_primary_columns()
            .iter()
            .map(|column| &column.specification.sql_type)
            .collect()
    }

    pub fn get_foreign_keys(&self) -> Vec<&ForeignKey> {
        let mut foreign: Vec<&ForeignKey> = vec![];
        for key in &self.table_key {
            match *key {
                TableKey::ForeignKey(ref fk) => foreign.push(fk),
                _ => (),
            }
        }
        foreign
    }

    pub fn get_foreign_key_to_table(&self, table_name: &TableName) -> Option<&ForeignKey> {
        let foreign_keys: Vec<&ForeignKey> = self.get_foreign_keys();
        for fk in foreign_keys {
            if fk.foreign_table == *table_name {
                return Some(fk);
            }
        }
        None
    }

    fn get_foreign_columns_to_table(&self, table_name: &TableName) -> Vec<&Column> {
        self.get_foreign_column_names_to_table(table_name)
            .iter()
            .filter_map(|column_name| self.get_column(column_name))
            .collect()
    }

    pub fn get_foreign_column_types_to_table(&self, table_name: &TableName) -> Vec<&SqlType> {
        self.get_foreign_columns_to_table(table_name)
            .iter()
            .map(|column| &column.specification.sql_type)
            .collect()
    }

    pub fn get_foreign_column_names_to_table(&self, table_name: &TableName) -> Vec<&ColumnName> {
        let mut foreign_columns = vec![];
        let foreign_keys = self.get_foreign_key_to_table(table_name);
        for fk in &foreign_keys {
            for fk_column in &fk.columns {
                foreign_columns.push(fk_column);
            }
        }
        foreign_columns
    }

    ///
    pub fn get_foreign_column_names(&self) -> Vec<&ColumnName> {
        let mut foreign_columns = vec![];
        let foreign_keys = self.get_foreign_keys();
        for fk in &foreign_keys {
            for fk_column in &fk.columns {
                foreign_columns.push(fk_column);
            }
        }
        foreign_columns
    }

    /// return the local columns of this table
    /// that is referred by the argument table name
    pub fn get_referred_columns_to_table(
        &self,
        table_name: &TableName,
    ) -> Option<&Vec<ColumnName>> {
        let foreign_keys: Vec<&ForeignKey> = self.get_foreign_keys();
        for fk in foreign_keys {
            if fk.foreign_table == *table_name {
                return Some(&fk.referred_columns);
            }
        }
        None
    }

    pub fn get_column(&self, column_name: &ColumnName) -> Option<&Column> {
        self.columns.iter().find(|c| c.name == *column_name)
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct PrimaryKey {
    pub name: Option<String>,
    pub columns: Vec<ColumnName>,
}

#[derive(Debug, PartialEq, Clone)]
pub struct UniqueKey {
    pub name: Option<String>,
    pub columns: Vec<ColumnName>,
}

/// example:
///     category { id, name }
///     product { product_id, name, category_id }
///
/// if the table in context is product and the foreign table is category
/// ForeignKey{
///     name: product_category_fkey
///     columns: [category_id]
///     foreign_table: category
///     referred_columns: [id]
/// }
#[derive(Debug, PartialEq, Clone)]
pub struct ForeignKey {
    pub name: Option<String>,
    // the local columns
    pub columns: Vec<ColumnName>,
    // referred foreign table
    pub foreign_table: TableName,
    // referred column of the foreign table
    // this is most likely the primary key of the table in context
    pub referred_columns: Vec<ColumnName>,
}

#[derive(Debug, PartialEq, Clone)]
pub struct Key {
    pub name: Option<String>,
    pub columns: Vec<ColumnName>,
}

#[derive(Debug, PartialEq, Clone)]
pub enum TableKey {
    PrimaryKey(PrimaryKey),
    UniqueKey(UniqueKey),
    Key(Key),
    ForeignKey(ForeignKey),
}

#[derive(Debug)]
pub struct SchemaContent {
    pub schema: String,
    pub tablenames: Vec<TableName>,
    pub views: Vec<TableName>,
}

#[cfg(test)]
mod test {
    use dao::TableName;
    use dao::ColumnName;
    use pool::Pool;
    
    #[test]
    fn referred_columns(){
        let db_url = "postgres://postgres:p0stgr3s@localhost:5432/sakila";
        let mut pool = Pool::new();
        let em = pool.em(db_url);
        assert!(em.is_ok());
        let em = em.unwrap();
        let film_tablename = TableName::from("public.film");
        let film = em.get_table(&film_tablename);
        let film_actor_tablename = TableName::from("public.film_actor");
        let film_actor = em.get_table(&film_actor_tablename);
        assert!(film.is_ok());
        println!("film: {:#?}", film);
        println!("FILM ACTOR {:#?}", film_actor);
        let film = film.unwrap();
        let film_actor = film_actor.unwrap();
        let rc = film_actor.get_referred_columns_to_table(&film.name);
        println!("rc: {:#?}", rc);
        assert_eq!(rc, 
            Some(&vec![ ColumnName {
                        name: "film_id".to_string(),
                        table: None,
                        alias: None
                    }
                ])
            );
    }
}
